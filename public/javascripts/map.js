var map = L.map('mapid').setView([6.1846099, -75.5991287], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: "json",
    headers: {
        'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlYzllZGM0YWU4MGY4MTBiYzg4YTZhMyIsImlhdCI6MTU5MDI5Mzk3NCwiZXhwIjoxNTkwODk4Nzc0fQ.3olIX-8JPSCRoWrP7TjO6cyCMT-IeemVT-57bgkZTQY'
    },
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        })
    }
})